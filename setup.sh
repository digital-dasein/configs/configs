#!/bin/sh

# SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
# SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
# SPDX-FileCopyrightText: 2022 Gerben Peeters    <gerben@digitaldasein.org>
#
# SPDX-License-Identifier: CC0-1.0

# config
CONFIG_DIR="$HOME"
VIM_DIR="$HOME/.vim"
OVERWRITE=0
ALL_VIM_EXTENSIONS=0
OLD_VERSIONS=0
YES_OPT=""
TMUX_REMOTE=0
SVB=0

TMUX_OLD="./archive/tmux.old.conf"

EXEC_PATH=$0
PROJ_DIR="$(dirname "${EXEC_PATH}")"

B_VIM=1
B_BASH=1
B_TMUX=1

usage()
{
    # In the here-doc (inside EOF - EOF):
    USAGE=$(
    cat << EOF | sed 's/\t\+\t/\t\t\t    /g'
Usage: $0 [-d DIR ] [-v DIR ] [-f] [-a] [...]

Options:
-d|--config-dir DIR     target directory general (default '$HOME')
-v|--vim-dir DIR        target directory vim (default '$HOME/.vim')
-f|--force-overwrite    force overwrite files and directories
-o|--older-versions     use config files compatible with older versions:
                            -  tmux: > v2 < v2.9
                            -  vim:  > v6
-nv|--no-vim            do not setup vim
-nb|--no-bash           do not setup bash
-nt|--no-tmux           do not setup tmux
-y|--assume-yes         do not prompt with dependency questions
-a|--all-vim-extensions enable all vim extensions (default: no extensinos)
-tr|--tmux-remote       use dark-blue bg-color for active tmux window, useful
                        for differentiating remote from local sessions
-svb|--custom-svb       add customised configs (based on svbaelen's personal
                        setup)
-h|--help|--usage       show help
EOF
)
printf "${USAGE}\n"
}

# options and positional arugments
while [ -n "$1" ]; do
    case "$1" in
        -h|--help|--usage)
            usage;
            exit 1
            ;;
        -f|--force-overwrite)
            OVERWRITE=1
            ;;
        -a|--all-vim-extensions)
            ALL_VIM_EXTENSIONS=1
            ;;
        -o|--older-versions)
            OLD_VERSIONS=1
            ;;
        -svb|--custom-svb)
            SVB=1
            ;;
        -y|--assume-yes)
            YES_OPT="-y"
            ;;
        -d|--config-dir) CONFIG_DIR=$2
            shift
            ;;
        -v|--vim-dir) VIM_DIR=$2
            shift
            ;;
        -tr|--tmux-remote) TMUX_REMOTE=1;;
        -nv|--no-vim) B_VIM=0 ;;
        -nb|--no-bash) B_BASH=0 ;;
        -nt|--no-tmux) B_TMUX=0 ;;
        -*|--*)
            printf "[ERROR] option not recognized.  Exiting...\n" usage;
            exit 1
            ;;
        *)
            # Default case: this is requred to obtain a positional arugment at $1
            # later (if desired)
            break
    esac
    shift
done

# check output dir
if [ ! -d "${CONFIG_DIR}" ]; then
    if [ "$YES_OPT" = "-y" ]; then
        answer="y"
    else
        printf "Directory '${CONFIG_DIR}' does not exist. "
        printf "Create it? (y/[n]) "
        read -r answer
    fi
    if [ "$answer" = "y" ];then
        mkdir -p "$CONFIG_DIR"
    else
        printf "OK, bye!\n"
    fi
fi

CP_OPTS="-ir"
VIM_OPTS=""
if [ $OVERWRITE = 1 ];then
    CP_OPTS="-rf"
    VIM_OPTS="$VIM_OPTS -f"
fi
if [ $ALL_VIM_EXTENSIONS = 1 ];then
    VIM_OPTS="$VIM_OPTS -a"
fi
if [ $OLD_VERSIONS = 1 ];then
    VIM_OPTS="$VIM_OPTS -o"
fi


#=====================================================================
# Main
#=====================================================================

cd "$PROJ_DIR"
git submodule init
git submodule update --remote --merge

# VIM
if [ $B_VIM = 1 ];then
    B_VIM_CMD=$(command -v vim)
    if [ -z "$B_VIM_CMD" ];then
        if [ "$YES_OPT" = "-y" ]; then
            answer="y"
        else
            printf "[WARNING] no vim installation found on your system\n"
            printf "Download vim? (y/[n]) "
            read -r answer
        fi
        if [ "$answer" = "y" ];then
            sudo apt update && sudo apt install $YES_OPT vim
        fi
    fi

    VIMRC_OUT="$CONFIG_DIR/.vimrc"

    ./vim/utils/setup.sh -d "${VIM_DIR}" \
        -ov "${VIMRC_OUT}" \
        $VIM_OPTS
            sleep 1
fi

# Bash
if [ $B_BASH = 1 ];then
    cp $CP_OPTS "./bash/bash_aliases" "${CONFIG_DIR}/.bash_aliases"
    cp $CP_OPTS "./bash/bash_paths"   "${CONFIG_DIR}/.bash_paths"
    cp $CP_OPTS "./bash/bash_functions"  "${CONFIG_DIR}/.bash_functions"
    if [ $SVB = 1 ];then
        cp $CP_OPTS "./bash/bash_svbaelen"  "${CONFIG_DIR}/.bash_svbaelen"
    fi
    printf "[INFO] Copied bash configs to '${CONFIG_DIR}'\n"
    if [ -e "${CONFIG_DIR}/.bashrc" ];then
        echo "source ${CONFIG_DIR}/.bash_aliases" >> ${CONFIG_DIR}/.bashrc
        echo "source ${CONFIG_DIR}/.bash_paths" >> ${CONFIG_DIR}/.bashrc
        echo "source ${CONFIG_DIR}/.bash_functions" >> ${CONFIG_DIR}/.bashrc
        if [ $SVB = 1 ];then
            echo "source ${CONFIG_DIR}/.bash_svbaelen" >> ${CONFIG_DIR}/.bashrc
        fi
        printf "[INFO] Added source commands in ${CONFIG_DIR}/.bashrc\n"
    else
        printf "No '.bashrc' file found in ${CONFIG_DIR}\n"
        printf "Please source bash files manually, e.g., in ~/.bash_profile.\n"
    fi
    printf "[INFO] Don't forget to source your bash scripts (e.g. $ source ~/.bashrc)\n"
    sleep 1
fi

# Tmux
if [ $B_TMUX = 1 ];then
    TMUX_FILE_O="${CONFIG_DIR}/.tmux.conf"

    if [ $OLD_VERSIONS = 1 ];then
        cp $CP_OPTS "./tmux/archive/tmux.old.conf" "$TMUX_FILE_O"
    else
        cp $CP_OPTS "./tmux/tmux.conf" "$TMUX_FILE_O"
    fi

    if [ $SVB = 1 ];then
        sed "s/#source-file/source-file/g" \
            "$TMUX_FILE_O" > "$TMUX_FILE_O.tmp" \
            && rm -f "$TMUX_FILE_O" && mv "$TMUX_FILE_O.tmp" "$TMUX_FILE_O"
    fi

    if [ $TMUX_REMOTE = 1 ];then
        sed "s/.*window-active-style.*/set -g window-active-style 'fg=colour15,bg=colour17'/g" \
            "$TMUX_FILE_O" > "$TMUX_FILE_O.tmp" \
            && rm -f "$TMUX_FILE_O" && mv "$TMUX_FILE_O.tmp" "$TMUX_FILE_O"
    fi
    printf "[INFO] copied tmux configs to '${CONFIG_DIR}'\n"
    B_TMUX_CMD=$(command -v tmux)
    if [ -z "$B_TMUX_CMD" ];then
        # install tmux if not found
        if [ "$YES_OPT" = "-y" ]; then
            answer="y"
        else
            printf "[WARNING] no tmux installation found on your system\n"
            printf "Download tmux? (y/[n]) "
            read -r answer
        fi
        if [ "$answer" = "y" ];then
            sudo apt update && sudo apt install $YES_OPT tmux
        fi
    fi
    printf "[INFO] Tmux setup finished. Use '$ tmux' to start a new session\n"
    sleep 1
fi

printf "[INFO] All done. Bye!\n"
