<!--

SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
SPDX-FileCopyrightText: 2022 Gerben Peeters    <gerben@digitaldasein.org>
SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>

SPDX-License-Identifier: CC0-1.0

-->

# Changelog

## [v0.0.2](https://gitlab.com/digital-dasein/configs/configs/tags/v0.0.2)

### Main features:

- Update to latest Vin and Bash versions

### Relevant commits

- c8d50df027e3b27d3df14a6884b51a531fa84e2f (**2022-03-17**) | Update to latest vim and bash versions (0.0.2) (*svbaelen*)
- 01cfd8f0727275dbf140be8dfea42687b60cba9b (**2022-02-23**) | Add CHANGELOG, and ensure reuse compliance (*svbaelen*)

## [v0.0.1](https://gitlab.com/digital-dasein/configs/configs/tags/v0.0.1)

### Main features
- include initial releases of `bash`, `vim`, and `tmux` configs
- automated setup with utility

### Relevant commits

- 96f5d601649883cad66571e9996e90ebdfdb71fb (**2022-02-16**) | update submods (*svbaelen*)
- f165118789d1835772271ec607e1d4ae3b551b5c (**2022-02-16**) | update README (*svbaelen*)
- 598e03179acf47568829b4e76759282e5396c7a1 (**2022-02-16**) | fix vim option error (*svbaelen*)
- 7494380632a0010469656905a5f249800ddbaa30 (**2022-02-16**) | allow for older config versions (*svbaelen*)
- c810b7727f6962f8452a7e3e43a7c3a671dcd07d (**2022-02-16**) | fix vim/tmux cmd checks (*svbaelen*)
- e2468a16c7dddcf23ac14d1562bed1268aa2f7a8 (**2022-02-16**) | add branch in submods (*svbaelen*)
- 0a39f4b1209f33858a3d7c36c871ca992bfebeb2 (**2022-02-16**) | fix submod path error (*svbaelen*)
- d9f933df90c11103aaa3d06f7fc32be85632a326 (**2022-02-16**) | add no-X options (*svbaelen*)
- e5013d1c7f3c2efb0fbdb9f526c2bfdc08a912f8 (**2022-02-16**) | update HEAD submods (*svbaelen*)
- 9c1b675c605794b3b0785f223049988877c0452b (**2022-02-16**) | utility added (*svbaelen*)
- 6c4a9cb03da81b68f69e203b9d510041adc5b622 (**2022-02-16**) | submods added (*svbaelen*)
