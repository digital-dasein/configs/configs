<!--


SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
SPDX-FileCopyrightText: 2022 Gerben Peeters    <gerben@digitaldasein.org>
SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>

SPDX-License-Identifier: CC0-1.0

-->

# Overarching configs

Including:

- [bash](https://gitlab.com/digital-dasein/configs/bash)
- [vim](https://gitlab.com/digital-dasein/configs/vim)
- [tmux](https://gitlab.com/digital-dasein/configs/tmux)

## Usage

### With utilities

(Download to `~/.config/` folder, and create folder if not existing)

```sh
$ export DDCONF_DIR="$HOME/.config/dd-configs" && mkdir -p "$DDCONF_DIR" \
   && git clone https://gitlab.com/digital-dasein/configs/configs $DDCONF_DIR \
   && $DDCONF_DIR/setup.sh
```

Or, for example using older config version, and avoid dependency install 
prompts:

```sh
$ git clone https://gitlab.com/digital-dasein/configs/configs \
    && ./configs/setup.sh -o -y
```

### Utility options

```sh
Usage: ./setup.sh [-d DIR ] [-v DIR ] [-f] [-a] [...]

Options:
   -d|--config-dir DIR     target directory general (default '$HOME')
   -v|--vim-dir DIR        target directory vim (default '$HOME/.vim')
   -f|--force-overwrite    force overwrite files and directories
   -o|--older-versions     use config files compatible with older versions:
                               -  tmux: > v2 < v2.9
                               -  vim:  > v6
   -nv|--no-vim            do not setup vim
   -nb|--no-bash           do not setup bash
   -nt|--no-tmux           do not setup tmux
   -y|--assume-yes         do not prompt with dependency install prompts
   -a|--all-vim-extensions enable all vim extensions (default: no extensinos)
   -svb|--custom-svb       add customised configs (based on svbaelen's personal
                           setup)
   -h|--help|--usage       show help
```

### Manually

```sh
$ cd /path/to/this/repo
$ git submodule init
$ git submodule update [--remote] [--merge]
```
(&rarr; follow instructions in the associated READMEs)


## Known issues

> `Unable to find current origin/master revision in submodule path [...]`

This is most likely caused by an old Git version that does not properly 
recognise `main` branches (as their default). This problem should be solved 
since e2468a16c7dddcf23ac14d1562bed1268aa2f7a8 (explicit setting of `main` 
branch), however, updating your Git might still be desirable.

> `several errors after initial (automatic) PlugInstall`

See: https://gitlab.com/digital-dasein/configs/configs/-/issues/1
